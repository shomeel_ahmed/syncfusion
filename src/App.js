 import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
// import "../node_modules/@syncfusion/ej2-react-buttons/styles/material.css";


import {Navigation} from './components/NavigationComponet/Navigation';
import {Home} from './components/HomeComponet/Home';
 import {About} from "./components/AboutComponet/About";


function App() {
  return (
      <BrowserRouter>
          <div className="container">
              {/*<h2>Header area</h2>*/}
              {/*<Navigation/>*/}
              <Home/>
              {/*<Switch>*/}
              {/*    <Route path='/' component={Home} exact/>*/}
              {/*    <Route path='/about' component={About} exact/>*/}
              {/*</Switch>*/}
          </div>
      </BrowserRouter>
  );
}

export default App;
