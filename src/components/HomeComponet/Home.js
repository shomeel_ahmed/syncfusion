import {ColumnDirective, ColumnsDirective, TreeGridComponent} from '@syncfusion/ej2-react-treegrid';
import {
    Filter,
    Inject,
    Page,
    Sort,
    Reorder,
    Resize,
    Selection,
    RowDD,
    ColumnMenu,
    Edit,
    ContextMenu,
    Toolbar,
    Freeze
} from '@syncfusion/ej2-react-treegrid';

import * as React from 'react';
import '../../App.css';

export class Home extends React.Component {
    constructor() {
        super(...arguments);
        this.toolbarOptions = ['Search'];
        this.editSettings = {allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Row'};
        this.sortingOptions = {
            columns: [
                {field: 'Category', direction: 'Ascending'},
                {field: 'orderName', direction: 'Ascending'}
            ]
        };
        this.settings = {cellSelectionMode: 'Box', type: 'Multiple', mode: 'Row'};
        this.ContextMenuItem = [
            'SortAscending',
            'SortDescending',
            'Edit',
            'Delete',
            {text: 'Copy', target: '.e-content', id: 'copy-val'},
            {text: 'Cut', target: '.e-content', id: 'cut-val'},
            {text: 'Paste', target: '.e-content', id: 'paste-val'},
            {text: 'Paste as Child', target: '.e-content', id: 'paste-val-child'},
            'Freeze',
            'AddRow',
        ];
        this.ContextMenuItem2 = [
            'SortAscending',
            'SortDescending',
            'Edit',
            'Delete',
            'Freeze',
            'AddRow',
        ];
        this.columnMenuItems = ['AutoFit', 'AutoFitAll', 'ColumnChooser',
            {text: 'Freeze', target: '.e-content', id: 'freeze'},
            {
                text: 'Alignment', items: [
                    {text: 'Right', target: '.e-content', id: 'align-right'},
                    {text: 'Left', target: '.e-content', id: 'align-left'},
                ]
            },
            {
                text: 'Font Color', items: [
                    {text: 'Font Red', target: '.e-content', id: 'font-red'},
                    {text: 'Font Blue', target: '.e-content', id: 'font-blue'},
                    {text: 'Font Green', target: '.e-content', id: 'font-green'},
                ]
            },

            {
                text: 'Font Size', items: [
                    {text: 'Font 12 px', target: '.e-content', id: 'font-12'},
                    {text: 'Font 14 px', target: '.e-content', id: 'font-14'},
                    {text: 'Font 16 px', target: '.e-content', id: 'font-16'}
                ]
            },
        ];
        this.columnMenuItems2 = ['AutoFit', 'AutoFitAll', 'ColumnChooser',
            {
                text: 'Alignment', items: [
                    {text: 'Right', target: '.e-content', id: 'align-right'},
                    {text: 'Left', target: '.e-content', id: 'align-left'},
                ]
            },
            {
                text: 'Font Color', items: [
                    {text: 'Font Red', target: '.e-content', id: 'font-red'},
                    {text: 'Font Blue', target: '.e-content', id: 'font-blue'},
                    {text: 'Font Green', target: '.e-content', id: 'font-green'},
                ]
            },

            {
                text: 'Font Size', items: [
                    {text: 'Font 12 px', target: '.e-content', id: 'font-12'},
                    {text: 'Font 14 px', target: '.e-content', id: 'font-14'},
                    {text: 'Font 16 px', target: '.e-content', id: 'font-16'}
                ]
            },
        ];
        this.state = {
            reRender: true,
            allowTextWrap: false,
            sortingEnabled: false,
            filtering: true,
            freeze: false,
            freezeNumber: 0,
            sortState: [],
            alignments: [],
            multiSelect: false,
            multiSelectArray: [],
            cutArray: [],
        }
    }

    componentDidMount() {
        const allState = {...this.state};
        const names = ['Seafood', 'Crystal', 'Diamond', 'Silver', 'Gold', 'Titanium', 'Platinum', 'Rubber', 'Plastic', 'Dry Fruits']
        let sortState = JSON.parse(JSON.stringify(allState.sortState));
        for (let i = 0; i < 1000; i++) {
            let obj = {
                orderID: i + 1,
                orderName: 'Order ' + i + 1,
                orderDate: new Date().setDate(new Date().getDate() + i),
                shippedDate: new Date().setDate(new Date().getDate() + i),
                units: '1395',
                unitPrice: '47.42',
                price: 134 + i,
                Category: names[i % 10],
                subtasks: []
            };
            sortState.push(obj)
        }
        this.setState({sortState})
    }

    columnMenuClick = (args) => {
        const id = args.item.id;
        switch (id) {
            case 'freeze':
                if (args.column.index != 0) {
                    this.setState({freezeNumber: args.column.index}, () => {
                        this.rerenderTable()
                    })
                }
                break;
            case 'font-red':
                document.getElementsByClassName("e-headertext")[args.column.index].style.color = 'red'
                this.changeStyle(args.column.index + 1, 'color', 'red');
                break;
            case 'font-blue':
                document.getElementsByClassName("e-headertext")[args.column.index].style.color = 'blue'
                this.changeStyle(args.column.index + 1, 'color', 'blue');
                break;
            case 'font-green':
                document.getElementsByClassName("e-headertext")[args.column.index].style.color = 'green'
                this.changeStyle(args.column.index + 1, 'color', 'green');
                break;
            case 'font-12':
                document.getElementsByClassName("e-headertext")[args.column.index].style.fontSize = '12px';
                this.changeStyle(args.column.index + 1, 'fontSize', '12px');
                break;
            case 'font-14':
                document.getElementsByClassName("e-headertext")[args.column.index].style.fontSize = '14px';
                this.changeStyle(args.column.index + 1, 'fontSize', '14px');
                break;
            case 'font-16':
                document.getElementsByClassName("e-headertext")[args.column.index].style.fontSize = '16px';
                this.changeStyle(args.column.index + 1, 'fontSize', '16px');
                break;
            case 'align-right': {
                const alignments = this.state.alignments;
                const find = alignments.indexOf(args.column.index);
                if(find == -1){
                    alignments.push(args.column.index);
                    this.setState({alignments}, ()=> this.rerenderTable())
                }
                break;
            }
            case 'align-left':{
                const alignments = this.state.alignments;
                const find = alignments.indexOf(args.column.index);
                if(find != -1){
                    alignments.splice(find, 1);
                    this.setState({alignments}, ()=> this.rerenderTable())
                }
                break;
            }
            default:
                break;
        }
    }

    changeStyle = (index, styleStr, value) => {
        const allRows = document.getElementsByClassName("e-row");
        for (let i = 0; i < allRows.length; i++) {
            const allCells = allRows[i].childNodes;
            allCells[index].style[styleStr] = value;
        }
    }

    rerenderTable = () => {
        this.setState({
            reRender: false
        })

        setTimeout(() => {
            this.setState({reRender: true});
        }, 0)
    }

    toggleAlignment = () => {
        this.setState({
            sortingEnabled: !this.state.sortingEnabled,
            reRender: false
        })

        setTimeout(() => {
            this.setState({reRender: true});
        }, 0)
    }

    toggleFiltering = () => {
        this.setState({
            filtering: !this.state.filtering,
        })
    }

    toggleTextWrap = () => {
        this.setState({
            allowTextWrap: !this.state.allowTextWrap,
        })
    }

    toggleMultiSelect = () => {
        this.setState({
            multiSelect: !this.state.multiSelect,
            multiSelectArray: [],
            cutArray: [],
            reRender: false
        })

        setTimeout(() => {
            this.setState({reRender: true});
        }, 0)
    }

    contextMenuClick = (args) => {
        const idx = args.rowInfo.rowIndex
        const id = args.item.id;
        const allState = {...this.state};
        let sortState = JSON.parse(JSON.stringify(allState.sortState));
        if (id == 'copy-val') {
            this.rerenderTable();
        } else if (id == 'cut-val') {
            const multiSelectArray = JSON.parse(JSON.stringify(this.state.multiSelectArray));
            for (let i = 0; i < multiSelectArray.length; i++) {
                sortState.splice(multiSelectArray[i].index - i, 1);
            }
            this.setState({sortState, multiSelectArray: [], cutArray: JSON.parse(JSON.stringify(multiSelectArray))});
            this.rerenderTable();
        } else if (id == 'paste-val') {
            let dataArray = JSON.parse(JSON.stringify(this.state.cutArray));
            let len = dataArray.length;
            if(dataArray.length == 0) {
                dataArray = JSON.parse(JSON.stringify(this.state.multiSelectArray));
                len = dataArray.length - 1;
            }
            if(len > 0){
                for (let i = 0; i < len; i++) {
                    delete dataArray[i].index;
                    sortState.splice(idx + i, 0, dataArray[i]);
                }
                this.setState({multiSelectArray: [], cutArray: [], sortState});
                this.rerenderTable();
            }
        } else if (id == 'paste-val-child') {
            let dataArray = JSON.parse(JSON.stringify(this.state.cutArray));
            if (dataArray.length == 0) dataArray = JSON.parse(JSON.stringify(this.state.multiSelectArray));
            if (dataArray.length > 0) {
                for (let i = 0; i < dataArray.length - 1; i++) {
                    delete dataArray[i].index;
                    sortState[idx].subtasks.push(dataArray[i]);
                }
                this.setState({multiSelectArray: [], cutArray: [], sortState});
            }
        }

    }

    rowDataBound = (args) => {
        const multiSelectArray = this.state.multiSelectArray;
        if (multiSelectArray.length > 0) {
            const find = multiSelectArray.findIndex((obj) => obj.index == args.data.index);
            if(find != -1)
                args.row.style.backgroundColor = 'green';
        } else {
            args.row.style.backgroundColor = '#ffffff';
        }
    }

    freezeFunction = () => {
        this.setState({
            freeze: !this.state.freeze,
            freezeNumber: 0,
        }, () => {
            this.rerenderTable()
        })
    }

    rowSelected = (args) => {
        const multiSelectArray = this.state.multiSelectArray;
        multiSelectArray.push(args.data);
        this.setState({multiSelectArray})
    }

    rowDeselected = (args) => {
        const multiSelectArray = this.state.multiSelectArray;
        const find = multiSelectArray.findIndex((obj)=> obj.index == args.data.index);
        if(find != -1){
            multiSelectArray.splice(find, 1);
            this.setState({multiSelectArray})
        }
    }

    checkAlignment = (index) => {
        const alignments = this.state.alignments;
        return (alignments.indexOf(index) != -1)? 'Right': 'Left';
    }

    render() {
        return <div className="container mt-5">
            <div className="table-responsive">
                <button
                    type="button" className="mb-2 btn btn-primary"
                    onClick={this.toggleAlignment}>{this.state.sortingEnabled ? 'Enable Copy/Cut/Paste' : 'Enable Sorting'}</button>
                <button type="button" className={' mb-2 ml-2 mb-2 btn btn-primary'}
                        onClick={this.toggleMultiSelect}>{this.state.multiSelect ? 'Disable Multiselect' : 'Enable Multiselect'}</button>
                <button type="button" className={' mb-2 ml-2 btn btn-primary'}
                        onClick={this.freezeFunction}>{this.state.freeze ? 'Disable Freze' : 'Enable Freeze'}</button>
                <button type="button" className={'  mb-2 ml-2 btn btn-primary'}
                        onClick={this.toggleTextWrap}>{this.state.allowTextWrap ? 'Disable TextWrap' : 'Allow TextWrap'}</button>
                <button type="button" className={'  mb-2 ml-2 btn btn-primary'}
                        onClick={this.toggleFiltering}>{this.state.filtering ? 'Disable Filtering' : 'Enable Filtering'}</button>
                {this.state.reRender &&
                <TreeGridComponent dataSource={this.state.sortState}
                                   treeColumnIndex={1}
                                   enableHover={false}
                                   allowTextWrap={this.state.allowTextWrap}
                                   pageSettings={{pageCount: 5}}
                                   childMapping='subtasks'
                                   allowPaging='true'
                                   allowResizing={true}
                                   frozenColumns={this.state.freezeNumber}
                                   allowSorting={this.state.sortingEnabled}
                                   allowRowDragAndDrop='true'
                                   selectionSettings={this.state.multiSelect ? this.settings : null}
                                   sortSettings={this.state.sortingEnabled ? this.sortingOptions : null}
                                   toolbar={this.state.filtering ? this.toolbarOptions : null}
                                   allowReordering={this.state.sortingEnabled}
                                   showColumnMenu={true}
                                   allowFiltering={this.state.filtering}
                                   filterSettings={{mode: 'Immediate', type: 'FilterBar', hierarchyMode: 'Parent'}}
                                   contextMenuItems={this.state.sortingEnabled ? this.ContextMenuItem2 : this.ContextMenuItem}
                                   editSettings={this.editSettings}
                                   ref={treegrid => this.treegrid = treegrid}
                                   contextMenuClick={this.contextMenuClick}
                                   rowDataBound={this.rowDataBound}
                                   columnMenuItems={this.state.freeze ? this.columnMenuItems : this.columnMenuItems2}
                                   columnMenuClick={this.columnMenuClick}
                                   rowSelected={this.rowSelected}
                                   rowDeselected={this.rowDeselected}
                >
                    <ColumnsDirective>
                        <ColumnDirective field='Category' headerText='Category' width='150' textAlign={this.checkAlignment(0)} />
                        <ColumnDirective field='orderName' headerText='Order Name' width='170' isPrimaryKey={true} textAlign={this.checkAlignment(1)} />
                        <ColumnDirective field='orderDate' headerText='Order Date' width='130' format='yMd'
                                         textAlign={this.checkAlignment(2)} type='date'/>
                        <ColumnDirective field='price' headerText='Price' width='100' type='number'
                                         format='C0' textAlign={this.checkAlignment(3)} />
                    </ColumnsDirective>
                    <Inject
                        services={[Page, Sort, Filter, Reorder, Resize, RowDD, Selection, ColumnMenu, Edit, Selection, ContextMenu, Toolbar, Freeze]}/>
                </TreeGridComponent>
                }
            </div>

        </div>
    }
}
