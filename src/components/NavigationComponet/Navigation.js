import React, {Component} from 'react';
import {BrowserRouter as Router, NavLink} from "react-router-dom";

export class Navigation extends Component {
    render() {
        return (
                <div className="home">
                    <h2>Navigation component</h2>
                    <NavLink to="/">HOME</NavLink>
                    <NavLink to="/about">ABOUT US</NavLink>
                </div>

        )}
}